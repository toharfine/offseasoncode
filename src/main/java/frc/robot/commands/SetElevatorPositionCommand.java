package frc.robot.commands;

import edu.wpi.first.wpilibj.command.Command;
import frc.robot.subsystems.Elevator;
/* 
 *  ---
 * /---\
  /-----\ !~~ T
 /-------\
 */

public class SetElevatorPositionCommand extends Command {
  private Elevator m_elevator;
  private int position;
  
  public SetElevatorPositionCommand() {
    this.m_elevator = m_elevator.getInstance();
  }

  @Override
  protected void initialize() {
    requires(this.m_elevator);
  }
  
  @Override
  protected void execute() {

  }

  
  @Override
  protected boolean isFinished() {
    return true;
  }

  @Override
  protected void end() {
    
  }

  @Override
  protected void interrupted() {
   }  
}
