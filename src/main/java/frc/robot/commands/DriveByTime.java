package frc.robot.commands;

import edu.wpi.first.wpilibj.command.Command;
import frc.robot.subsystems.Driver;

public class DriveByTime extends Command {
  private Driver driver;
  private boolean isForward;
  private double time;

  public DriveByTime(double time, boolean isForward) {
    this.driver = Driver.getInstance();
    this.isForward = isForward;
    this.time = time;

  }

  @Override
  protected void initialize() {
    System.out.println("I have started /////////////////////////////////");
    setTimeout(time);

    
  }

  @Override
  protected void execute() {
    if(this.isForward){
      this.driver.arcadeDrive(0.6, 0);

    }
    else{
      this.driver.arcadeDrive(-0.6, 0);

    }

    System.out.println("still runnin");
  }

  @Override
  protected boolean isFinished() {
    return isTimedOut();
  }

  @Override
  protected void end() {
    driver.stopAllWheels();
  }

  @Override
  protected void interrupted() {
    driver.stopAllWheels();

  }
}
