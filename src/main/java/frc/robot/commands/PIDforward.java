package frc.robot.commands;

import edu.wpi.first.wpilibj.command.Command;
import frc.lib.PrimoLib.TwoPIDControllerWrapper;
import frc.robot.subsystems.Driver;

public class PIDforward extends Command {

  private Driver driver;
  private double target;
  private TwoPIDControllerWrapper controller;
  
  public PIDforward(double position) {
    this.driver = Driver.getInstance();
    this.target = position;
    this.controller = new TwoPIDControllerWrapper(1, 0, 0, this.driver.getPidSourceEncoder(), 1, 0, 0, this.driver.getPidSourceGyro(), new Runnable(){
    
      @Override
      public void run() {
        if(controller.onTarget1()) controller.stop(); 
        driver.arcadeDrive(controller.getPIDOutput1(), controller.getPIDOutput2());
      }
    });
  }
    

  @Override
  protected void initialize() {
    requires(this.driver);
    controller.init(target, 10, 0, 1);
    setTimeout(3);
    controller.enable(); 
  }

  @Override
  protected void execute() {
  }

  @Override
  protected boolean isFinished() {
    return controller.onTarget1()||isTimedOut();
  }

  @Override
  protected void end() {
    controller.close();
  }

  @Override
  protected void interrupted() {
    end();
  }
}