package frc.lib.PrimoLib;

import edu.wpi.first.wpilibj.Notifier;
import edu.wpi.first.wpilibj.PIDController;
import edu.wpi.first.wpilibj.PIDOutput;
import edu.wpi.first.wpilibj.PIDSource;

public class TwoPIDControllerWrapper extends Notifier{

    private PIDController controller1;
    private PIDController controller2;

    public TwoPIDControllerWrapper(double Kp1, double Ki1, double Kd1, 
    PIDSource source1, double Kp2, double Ki2, double Kd2,  
    PIDSource source2, Runnable pidWrite){
        super(pidWrite);
        this.controller1 = new PIDController(Kp1, Ki1, Kd1, source1, new PIDOutput(){
            @Override
            public void pidWrite(double output) {
                
            }
        });
        this.controller2 = new PIDController(Kp2, Ki2, Kd2, source2, new PIDOutput(){
            @Override
            public void pidWrite(double output) {
                
            }
        });

    }



    public boolean onTarget1(){
        return this.controller1.onTarget();
    }
    
    public boolean onTarget2(){
        return this.controller2.onTarget();
    }
    

    public double getPIDOutput1(){
        return this.controller1.get();
    }

    
    public double getPIDOutput2(){
        return this.controller2.get();
    }

    public void init(double setPoint1, double setAbsoluteTolerance1, double setPoint2, double setAbsoluteTolerance2){
        this.controller1.setOutputRange(-1, 1);
        this.controller1.setContinuous(false);
        this.controller1.setSetpoint(setPoint1);
        this.controller1.setAbsoluteTolerance(setAbsoluteTolerance1);
        this.controller2.setOutputRange(-1, 1);
        this.controller2.setContinuous(false);
        this.controller2.setSetpoint(setPoint2);
        this.controller2.setAbsoluteTolerance(setAbsoluteTolerance2);
    }

    public void enable(){
        this.controller1.enable();
        this.controller2.enable();
        startPeriodic(0.02);
    }

    

    // recommended to use an @if statement like that:
    // if (onTarget1()&&onTarget2()){TwoPIDControllerWrapper.stop();} 
    


}
