package frc.robot.commands;

import edu.wpi.first.wpilibj.command.Command;
import frc.robot.Robot;
import frc.robot.subsystems.Driver;

public class TurnByTime extends Command {
 private Driver driver;
 private double time;
 private boolean rightRotating;

  public TurnByTime(double tm, boolean b) {
    this.driver = Robot.driver;
    this.time = tm;
    this.rightRotating = b;
   }

  @Override
  protected void initialize() {
    setTimeout(time);
    
  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
    System.out.println("running");
    if(rightRotating){
      driver.arcadeDrive(0, 0.67);
      }
      else
      {
        driver.arcadeDrive(0, -0.67);
      }
  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    return isTimedOut();
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
  driver.stopAllWheels();
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted()
  {
    driver.stopAllWheels();
  }
}
