/*----------------------------------------------------------------------------*/
/*                                                                            */
/*                                   O/I                                      */
/*                                 GEVALD!!!                                  */
/*                                                                            */
/*----------------------------------------------------------------------------*/

package frc.robot;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.buttons.JoystickButton;
import frc.robot.commands.SetElevatorPositionCommand;
import frc.robot.subsystems.Elevator;

public class OI {

    //Driver Joystick & Buttons
    public Joystick joystickDriver;
    public JoystickButton a; //Switch Cameras
    public JoystickButton b; //Push Panel Arm
    public JoystickButton x; //Activate Vacum
    public JoystickButton y; //Vision Align
    public JoystickButton rb; //Catch the cargo
    public JoystickButton lb; //Shoot the cargo
    public JoystickButton start; //FuckGoBack; //who the hell comments like that
     
    //Console Joystick & Buttons
    public Joystick consoleOperator; //joystickOperator
    public JoystickButton consoleBlue1; //Elevator move to floor
    public JoystickButton consoleBlue2; //Elevator move to low panel
    public JoystickButton consoleBlue3; //Elavator move to middle panel
    public JoystickButton consoleRed1; //Elevator move to low cargo
    public JoystickButton consoleRed2; //Gripper inside

    //Operator Joystick & Buttons
    public Joystick joystickOperator;
    public JoystickButton btn6; //Compressor
    public JoystickButton btn5; //All solenoids are opening
    public JoystickButton btn3; //Back solenoids are closing
    public JoystickButton btn8;
    public JoystickButton btn7;
    public JoystickButton btn9;

    public OI(){
        //This sets the buttons to their location on the joystick
    joystickDriver = new Joystick(0);
    a = new JoystickButton(joystickDriver,1); 
    b = new JoystickButton(joystickDriver,2);
    x = new JoystickButton(joystickDriver,3);
    y = new JoystickButton(joystickDriver,4);
    rb = new JoystickButton(joystickDriver,5);
    lb = new JoystickButton(joystickDriver,6);
    start = new JoystickButton(joystickDriver, 7);

    joystickOperator = new Joystick(2);
    btn6 = new JoystickButton(joystickOperator,8);
    btn5 = new JoystickButton(joystickOperator,4);
    btn3 = new JoystickButton(joystickOperator,6);
    btn8 = new JoystickButton(joystickOperator, 5);
    btn7 = new JoystickButton(joystickOperator, 7);
    btn9 = new JoystickButton(joystickOperator, 3);
    
    consoleOperator = new Joystick(1);
    consoleBlue1 = new JoystickButton(consoleOperator, 3);//low
    consoleBlue2 = new JoystickButton(consoleOperator, 2);//mid
    consoleBlue3 = new JoystickButton(consoleOperator, 1);//high 
    consoleRed1 = new JoystickButton(consoleOperator,5);//openLoop
    consoleRed2 = new JoystickButton(consoleOperator,4);// hatches / cargos
    




    







    //This connects our buttons to the commands we want
    // x.whenPressed(new VacuumMaker());
    // rb.whileHeld(new ShootCargo());
    // lb.whileHeld(new CargoCollector());

    // btn9.whenPressed(new ElevatorClimbPosition());
    // btn6.whenPressed(new SwitchCompressor());
    // //btn5.whenPressed(new SwitchAllSolenoids()); 
    // //btn3.whenPressed(new SwitchFrontSolenoids());
    // //btn8.whenPressed(new SwitchBackSolenoids()); //SwitchBackSolenoids

    // consoleA.whenPressed(new MoveGripperInside());
    // consoleD.whenPressed(new MoveGripperToMid());
    // consoleE.whenPressed(new MoveGripperToFloor());
    // consoleB.whenPressed(new AutoElevatorToCatchPanel());
    // consoleF.whenPressed(new AutoElevatorToCatchCargo()); 
    }



  
}
