package frc.robot.commands;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.OI;
import frc.robot.Robot;
import frc.robot.subsystems.Driver;

public class ArcadeDrive extends Command {
  private Driver driver; 
  private OI oi;
  private double speed;
  private double rotation;
  private double k; //speed from Smartdashboard

  public ArcadeDrive() {
    this.driver = Robot.driver;
    this.oi = Robot.m_oi;
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
    requires(this.driver);
  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
    this.speed = (this.oi.joystickDriver.getRawAxis(1) * SmartDashboard.getNumber("MaxDrivingSpeed", 0.7));
    this.rotation = (this.oi.joystickDriver.getRawAxis(4) * SmartDashboard.getNumber("MaxRotationSpeed", 0.7));
    k = 1 - SmartDashboard.getNumber("MaxDrivingSpeed", 0.7);

    if (Math.abs(speed) <= 0.2) {
      speed = 0;
    }
    if (Math.abs(rotation) <= 0.2) {
      rotation = 0;
    }

    if (Math.abs(oi.joystickDriver.getRawAxis(2)) > 0.3)
    {
      this.speed *= 0.55/SmartDashboard.getNumber("MaxDrivingSpeed", 0.7);
      this.rotation *= 0.65/SmartDashboard.getNumber("MaxRotationSpeed", 0.7);
    }
    if (Math.abs(oi.joystickDriver.getRawAxis(3)) > 0.3)
    {
      this.speed = this.oi.joystickDriver.getRawAxis(1);
      this.rotation = this.oi.joystickDriver.getRawAxis(4);
    }
      this.driver.arcadeDrive(-speed, rotation);
  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    return false;
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
    this.driver.stopAllWheels();
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
    this.driver.stopAllWheels();
  }
}
