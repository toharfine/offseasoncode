package frc.robot.subsystems;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import com.ctre.phoenix.motorcontrol.can.WPI_VictorSPX;
import com.kauailabs.navx.frc.AHRS;
import edu.wpi.first.wpilibj.PIDSource;
import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import frc.lib.PrimoLib.PIDsourceWrapper;
import frc.robot.RobotMap;

public class Driver extends Subsystem {
  private WPI_TalonSRX driverFrontRight;
  private WPI_TalonSRX driverFrontLeft;
  private WPI_VictorSPX driverFollowerRight;
  private WPI_VictorSPX driverFollowerLeft;
  private AHRS gyro;
  private SpeedControllerGroup rightController;
  private SpeedControllerGroup leftController;
  private DifferentialDrive diffDrive;
  private PIDSource pidSourceEncoder;
  private PIDSource pidSourceNavX;

  private static Driver instance = null;

  private Driver(){

  this.driverFrontRight = new WPI_TalonSRX(RobotMap.DriverPorts.driverFrontRight);
  this.driverFrontLeft = new WPI_TalonSRX(RobotMap.DriverPorts.driverFrontLeft);
  this.driverFollowerRight= new WPI_VictorSPX(RobotMap.DriverPorts.driverFollowerRight);
  this.driverFollowerLeft = new WPI_VictorSPX(RobotMap.DriverPorts.driverFollowerLeft);
  this.driverFollowerLeft.follow(this.driverFrontLeft);
  this.driverFollowerRight.follow(this.driverFrontRight);
  this.rightController = new SpeedControllerGroup(this.driverFrontRight);
  this.leftController = new SpeedControllerGroup(this.driverFrontLeft);
  this.diffDrive = new DifferentialDrive(this.leftController, this.rightController);
  
  this.pidSourceEncoder = new PIDsourceWrapper(){
    @Override
    public double pidGet() {
      return getLeftEncoderPosition();
    }
  };
  this.pidSourceNavX = new PIDsourceWrapper(){
  
    @Override
    public double pidGet() {
      return 0;
    }
  };
}
public PIDSource getPidSourceGyro(){
  return this.pidSourceNavX;
}

public PIDSource getPidSourceEncoder(){
  return this.pidSourceEncoder;
}

public double getGyroAngle(){
  return this.gyro.getAngle();
}

public void resetGyro(){
  this.gyro.reset();
}

public double getSpeedRight(){
  return this.driverFrontRight.getSelectedSensorVelocity();
}

public double getSpeedLeft(){
  return this.driverFrontLeft.getSelectedSensorVelocity();
}

public static Driver getInstance(){
  if(instance == null){
    instance = new Driver();
  }

  return instance;

}

public double getRightEncoderPosition(){
  return -this.driverFrontRight.getSelectedSensorPosition();
}
public double getLeftEncoderPosition(){
  return this.driverFrontLeft.getSelectedSensorPosition();
}

public void resetEncoders(){
  this.driverFrontLeft.setSelectedSensorPosition(0,0,10);
  this.driverFrontRight.setSelectedSensorPosition(0,0,10);
}

public void setSpeedRight(double speed)
{
  this.driverFrontRight.set(-speed);
}

public void setSpeedLeft(double speed)
{
  this.driverFrontLeft.set(speed);
}

public void stopAllWheels()
{
  this.driverFrontLeft.set(0);
  this.driverFrontRight.set(0);
}

public void arcadeDrive(double speed, double rotation)
{
  this.diffDrive.arcadeDrive(speed, rotation);
}

  @Override
  public void initDefaultCommand() {
  }

}






