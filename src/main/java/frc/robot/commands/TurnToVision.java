/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.wpilibj.command.Command;
import frc.robot.Robot;
import frc.robot.subsystems.Driver;

public class TurnToVision extends Command {
  double speed; 
  double setPoint;
  double error;
  double prevError;
  double p = 1, d = 1;
  double ma, derivative;
  Driver driver;
  public TurnToVision() {
    driver = Driver.getInstance();
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
    Robot.Navx.reset();
    this.setPoint = Robot.table.getEntry("angle").getDouble(0);
    this.error = 0;
    this.prevError = 0;
    this.derivative = 0;
  }
  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {

    System.out.println(ma);
    this.error = setPoint - Robot.Navx.getAngle();
    this.derivative = (this.error - this.prevError)/0.02;
    this.ma = this.error * this.p + this.derivative * this.d; 
    this.driver.arcadeDrive(0, ma);
    this.prevError = this.error;
  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    return false;
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
  }
}
