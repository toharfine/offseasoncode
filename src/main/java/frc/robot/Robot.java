/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;
 
import java.io.IOException;
import java.util.ArrayList;

import com.kauailabs.navx.frc.AHRS;
import edu.wpi.first.wpilibj.SPI;

import edu.wpi.cscore.UsbCamera;
import edu.wpi.cscore.VideoCamera;
import edu.wpi.cscore.VideoMode.PixelFormat;
import edu.wpi.first.cameraserver.CameraServer;
import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.I2C;
import edu.wpi.first.wpilibj.Notifier;
import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.GenericHID.RumbleType;
import edu.wpi.first.wpilibj.SerialPort.Port;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.Scheduler;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.Utils.Constants;
import frc.robot.Utils.HandleCameras;
import frc.robot.commands.ArcadeDrive;
import frc.robot.commands.LiftByJoystick;
import frc.robot.commands.MoveGripperByJoystick;
import frc.robot.subsystems.Driver;
import frc.robot.subsystems.Elevator;
import frc.robot.subsystems.Gripper;


public class Robot extends TimedRobot {
  // Motion.m_follower_notifier.stop();
  //this table handles vision values
  public static NetworkTable table;
  public static OI m_oi;
  public static DriverStation ds;
  //our Subsystems ->
  public static Gripper gripper;
  public static Driver driver;
  public static Elevator elevator;
  //these are command flags, that help us stop commands under the scheduler
  public static boolean commandFlag;
  public static boolean vacuumFlag;
  public static boolean visionFlag;
  public static boolean disableVacumSwitch;
  public static boolean climbingFlag;
  public static boolean climbSolFlag;
  public static HandleCameras handleCameras;
  public static I2C i2c;
  public static AHRS Navx;
  static Notifier m_follower_notifier;
  
  @Override
  public void robotInit() {
    ds = m_ds;
    this.climbSolFlag = false;
    //Connects the subsystems to the RobotMap motors and sensors.
    table = NetworkTableInstance.getDefault().getTable("imgProc");
    // gripper = new Gripper(RobotMap.moveGripper, RobotMap.rollerGripper, RobotMap.opticalEncoder, RobotMap.pushPanel,  RobotMap.vacSol, RobotMap.gripperSwitch);
    driver = Driver.getInstance();
    gripper = Gripper.getInstance();
    elevator = new Elevator();

    //Sets all the flags to false so we could use the schedulers
    gripper.setPanelPush(false);
    disableVacumSwitch = false;
    commandFlag = false;
    visionFlag = false;
    vacuumFlag = false;
    climbingFlag = false;
    initCameras();
    elevator.resetEncoder();
    UsbCamera cam0 = CameraServer.getInstance().startAutomaticCapture(0);
    cam0.setPixelFormat(PixelFormat.kYUYV);
    //cam0.setVideoMode(PixelFormat.kMJPEG, 320, 240, 30);
    //UsbCamera cam1 = CameraServer.getInstance().startAutomaticCapture(1);
    //cam1.setVideoMode(PixelFormat.kMJPEG, 320, 240, 30);
    m_oi = new OI();
    m_oi.joystickDriver.setRumble(RumbleType.kRightRumble, 0);
    m_oi.joystickDriver.setRumble(RumbleType.kLeftRumble, 0);



  }
  

  @Override
  public void robotPeriodic() {
    
  }

  @Override
  public void disabledInit() {
  }

  @Override
  public void disabledPeriodic() {
    Scheduler.getInstance().run();
  }

  
  @Override
  public void autonomousInit() {
    
  }

  private static void followPath() {
  }

  @Override
  public void autonomousPeriodic() {
    SmartDashboardPerodic();

  }

  @Override
  public void teleopInit() {
    Scheduler.getInstance().add(new ArcadeDrive());
    Scheduler.getInstance().add(new LiftByJoystick());
    driver.resetEncoders();
    SmartDashboardInit();
  }

  @Override
  public void teleopPeriodic() {
    //same as the auto function
    Scheduler.getInstance().run();
    SmartDashboardPerodic();
  }

  @Override
  public void testPeriodic() {
  }

  //These are values we want to define during our practice
  public void SmartDashboardInit() {
    SmartDashboard.putNumber("TurnTarget", 90);


  }
  //These are values we want to see post-match 
  public void SmartDashboardPerodic() {
    SmartDashboard.putNumber("right position", driver.getRightEncoderPosition());
    SmartDashboard.putNumber("left position", driver.getLeftEncoderPosition());
    
  }

  public void initCameras() {
    //This command connects our cameras to the MJPEG server
    ArrayList<VideoCamera> cams = new ArrayList<>();
    for (int i = 0; i < 2; i++) {
      UsbCamera cam = new UsbCamera("cam" + i, i);
      cam.setVideoMode(PixelFormat.kMJPEG, 320, 240, 30);
      cams.add(cam);
    }
    handleCameras = new HandleCameras(cams);
  }
}