package frc.robot.subsystems;

import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.command.Subsystem;
import frc.robot.RobotMap;
import frc.robot.Utils.Constants;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.StatusFrame;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import com.ctre.phoenix.motorcontrol.can.WPI_VictorSPX;

public class Elevator extends Subsystem {

  GamePieceHandler handler;
  
  ///////////////////////////////////////////////////////////////////////////
  
  public static enum ElevatorStates{
    openLoop, disabled, positionControl
  }
  ////////////////////////////////////////////////////////////
  
  public static enum GamePieceHandler{
    Cargo, Hatch, empty
  }
  public static enum ElevatorPositions{
    ROCKET_LOW, ROCKET_MIDDLE, ROCKET_HIGH, FEEDER, CARGOSHIP, CLOSE
  }



  private WPI_TalonSRX elevatorTalonR;
  private WPI_VictorSPX elevatorFollowerVictorR;
  private WPI_TalonSRX elevatorTalonL;
  private WPI_VictorSPX elevatorFollowerVictorL;
  private Compressor compressor;
  private DigitalInput elevatorMicFloor;
  private double heightFromFloor;
  private static Elevator instance = null;
  private static Elevator.ElevatorStates elevatorState;
 
  // Constractor
  public Elevator()  {
    //sensor
    this.elevatorMicFloor = new DigitalInput(RobotMap.ElevatorPorts.elevatorMicFloor);
    
    handler = GamePieceHandler.empty;
    //motors
    this.elevatorTalonL = new WPI_TalonSRX(RobotMap.ElevatorPorts.elevatorTalonL);
    this.elevatorTalonR = new WPI_TalonSRX(RobotMap.ElevatorPorts.elevatorTalonR);
    this.elevatorFollowerVictorL = new WPI_VictorSPX(RobotMap.ElevatorPorts.elevatorFollowerVictorL);
    this.elevatorFollowerVictorR = new WPI_VictorSPX(RobotMap.ElevatorPorts.elevatorFollowerVictorR);

    this.elevatorTalonL.follow(this.elevatorTalonR);
    this.elevatorFollowerVictorL.follow(this.elevatorTalonR);
    this.elevatorFollowerVictorR.follow(this.elevatorTalonR);
    
    this.elevatorTalonR.configFactoryDefault();
    this.elevatorTalonL.configFactoryDefault();
    this.elevatorFollowerVictorL.configFactoryDefault();
    this.elevatorFollowerVictorR.configFactoryDefault();

    
    this.elevatorTalonR.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Relative);
    this.elevatorTalonR.setSensorPhase(true);

    this.elevatorTalonR.configNominalOutputForward(0, Constants.ElevatorConstants.time_out);
    this.elevatorTalonR.configNominalOutputReverse(0, Constants.ElevatorConstants.time_out);

    this.elevatorTalonR.configPeakOutputForward(1, Constants.ElevatorConstants.time_out);
    this.elevatorTalonR.configPeakOutputReverse(1, Constants.ElevatorConstants.time_out);


    this.elevatorTalonR.setStatusFramePeriod(StatusFrame.Status_10_MotionMagic, 30);
    this.elevatorTalonR.selectProfileSlot(0, 0);
    this.elevatorTalonR.config_kF(0, 0.28,30);
    this.elevatorTalonR.config_kP(0, 0.2, 30);
    this.elevatorTalonR.config_kI(0, 0, 30);
    this.elevatorTalonR.config_kD(0, 0, 30);
    this.elevatorTalonR.configMotionAcceleration(4255, 30);
    this.elevatorTalonR.configMotionCruiseVelocity(4255, 30);//reall max 4255
    this.elevatorTalonR.setSelectedSensorPosition(0,0,30);


    this.compressor = new Compressor(RobotMap.ElevatorPorts.compressor);
    // this.elevatorMicFloor = new DigitalInput(RobotMap.ElevatorPorts.elevatorMicFloor);
    elevatorState = ElevatorStates.disabled;
  }


  public void setElevatorPositionMotionMagic(int posi){
    this.elevatorTalonR.set(ControlMode.MotionMagic, posi);
  }
  
  public void setElevatorState(ElevatorStates s){
    elevatorState = s;
  }
  
  public boolean isOnMicro(){
    return this.elevatorMicFloor.get();
  }
  

  @Override
  public void periodic() {
    if(isOnMicro()){
      resetEncoder();
    }
  }
  public ElevatorStates getElevatorState(){
    return elevatorState;
  }

  public static Elevator getInstance(){
    if(instance == null){
      instance = new Elevator();
    }
    return instance;
  } 

  public double getEncoderPosition() {
    return this.elevatorTalonR.getSelectedSensorPosition(0); // * CONVERT_TICKS_TO_METER;
  }

  public double getEncoderVelocity() {
    return this.elevatorTalonR.getSelectedSensorVelocity(0);
  }

  public void resetEncoder() {
    this.elevatorTalonR.setSelectedSensorPosition(0, 0, 10);
  }

  // Set elevator speed
  public void setElevatorSpeed(double speed) {
    this.elevatorTalonR.set(speed);
  }

  // Get elevator speed
  public double getElevatorSpeed() {
    return this.elevatorTalonR.get();
  }
  public boolean getCompressorStatus() {
    return this.compressor.enabled();
  }

  public void stopElevatorMotors() {
    this.elevatorTalonR.set(0);
    this.elevatorTalonL.set(0);
  }
 
  @Override
  public void initDefaultCommand() {
  }

  public static int getElevatorDesiredPosition(GamePieceHandler handled, ElevatorPositions position){
    //handled <-- GamePiceceHandler.state
    switch(position){
      case CARGOSHIP: 
        switch(handled){
          case Cargo: return Constants.ElevatorConstants.CARGOSHIP_CARGO;
          case Hatch: return Constants.ElevatorConstants.CARGOSHIP_PANEL;
          case empty: return 0;
    }
     case CLOSE: 
      return 0;      
     case FEEDER:
     switch(handled){
       case Cargo: return Constants.ElevatorConstants.FEEDER_CARGO;
       case Hatch: return Constants.ElevatorConstants.FEEDER_PANEL;
       case empty: return 0;
     }
     case ROCKET_HIGH:    
     switch(handled){
      case Cargo: return Constants.ElevatorConstants.ROCKET_HIGH_CARGO;
      case Hatch: return Constants.ElevatorConstants.ROCKET_HIGH_HATCH;
      case empty: return 0;
       

}
     case ROCKET_LOW:    
     switch(handled){
      case Cargo: return Constants.ElevatorConstants.ROCKET_LOW_CARGO;
      case Hatch: return Constants.ElevatorConstants.ROCKET_LOW_HATCH;
      case empty: return 0;
}
     case ROCKET_MIDDLE:    
     switch(handled){
      case Cargo: return Constants.ElevatorConstants.ROCKET_MID_CARGO;
      case Hatch: return Constants.ElevatorConstants.ROCKET_MID_HATCH;
      case empty: return 0;
}
    default: return 0;
    }
  }


}