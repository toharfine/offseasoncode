/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import com.kauailabs.navx.frc.AHRS;

import edu.wpi.first.wpilibj.command.Command;
import frc.robot.Robot;
import frc.robot.subsystems.Driver;

public class PeeEyeDolev extends Command {
  Driver driver;
  AHRS m_NavX;
  double kP, kI, kD, error, prevError, setPoint;

  public PeeEyeDolev() {
     driver = Driver.getInstance();
     m_NavX = Robot.Navx;
     kP = 0;kI = 0;kD = 0;
  }

  @Override
  protected void initialize() {
  }

  @Override
  protected void execute() {
    error = setPoint - m_NavX.getAngle();

  }

  @Override
  protected boolean isFinished() {
    return false;
  }

  @Override
  protected void end() {
  }

  @Override
  protected void interrupted() {
  }
}
