package frc.robot.commands.ElevatorAutos;

import edu.wpi.first.wpilibj.PIDController;
import edu.wpi.first.wpilibj.PIDOutput;
import edu.wpi.first.wpilibj.PIDSource;
import edu.wpi.first.wpilibj.PIDSourceType;
import edu.wpi.first.wpilibj.command.Command;
import frc.robot.subsystems.Elevator;
import frc.robot.subsystems.Elevator.ElevatorStates;

public class MoveElevatorPID extends Command {
  private Elevator elevator;
  private double target;
  private PIDController pc;
  
  public MoveElevatorPID(double position) {
    this.elevator = Elevator.getInstance();
    this.target = position;
    }

      

  @Override
  protected void initialize() {
    this.pc = new PIDController(1, 0, 0, 0, new PIDSource(){
    
      @Override
      public void setPIDSourceType(PIDSourceType pidSource) {

      }
    
      @Override
      public double pidGet() {
        return Elevator.getInstance().getEncoderPosition();
      }
    
      @Override
      public PIDSourceType getPIDSourceType() {
        return PIDSourceType.kDisplacement;
      }
    }, new PIDOutput(){
    
      @Override
      public void pidWrite(double output) {
        Elevator.getInstance().setElevatorSpeed(output);
      }
    },0.02);

    
    requires(this.elevator);
    this.pc.setInputRange(-1, 1);
    this.elevator.setElevatorState(ElevatorStates.positionControl);
    this.pc.setContinuous(false);
    this.pc.setSetpoint(this.target);
    this.pc.setAbsoluteTolerance(2);
    this.pc.enable();
    setTimeout(3);

  }

  @Override
  protected void execute() {
  }

  @Override
  protected boolean isFinished() {
    return this.pc.isEnabled()|| isTimedOut();
  }

  @Override
  protected void end() {
  }

  @Override
  protected void interrupted() {
  }
}
